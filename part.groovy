import groovy.json.JsonSlurper


def slurper = new JsonSlurper()

def outFlowFiles = [:]

def inFlowFile = session.get()
if (!inFlowFile) return

def tableName = inFlowFile.getAttribute('tableName')
def timeColumn = inFlowFile.getAttribute('timeColumn')

session.read(inFlowFile, { inStream -> 

    def reader = new BufferedReader(new InputStreamReader(inStream))

    while ((line = reader.readLine()) != null) {

        def doc = slurper.parseText(line)
        def timestamp = doc[timeColumn]
            
        def matcher = timestamp =~ /(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2}) \d{2}:\d{2}:\d{2}(\.\d+)?/

        matcher.matches()

        def year  = matcher.group('year')
        def month = matcher.group('month')
        def day   = matcher.group('day')
        def key   = String.format('%s-%s%s%s', tableName, year, month, day)

        if (!outFlowFiles.containsKey(key)) {
            outFlowFile = session.create()

            outFlowFile = session.putAttribute(outFlowFile, 'year',  year)
            outFlowFile = session.putAttribute(outFlowFile, 'month', month)
            outFlowFile = session.putAttribute(outFlowFile, 'day',   day)
            outFlowFile = session.putAttribute(outFlowFile, 'tableName',  tableName)  

            outFlowFiles.put(key, outFlowFile)
        }

        def outFlowFile = outFlowFiles.get(key)

        outFlowFile = session.append(outFlowFile, { out ->
            out.write(String.format('%s\n', line).getBytes())
        } as OutputStreamCallback)
    }

    outFlowFiles.each { key, value ->
        session.transfer(value, REL_SUCCESS)
    }

} as InputStreamCallback)


session.remove(inFlowFile)
