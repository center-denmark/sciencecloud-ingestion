import org.apache.nifi.controller.ControllerService
import org.apache.nifi.components.state.Scope
import java.text.SimpleDateFormat
import java.util.Date
import groovy.sql.Sql
import groovy.json.JsonOutput


def genDate(year=1, month=1, day=1, hour=0, minute=0, second=0, millisecond=0) {
    Calendar cal = Calendar.getInstance()

    cal.set(Calendar.YEAR, year)
    cal.set(Calendar.MONTH, month-1)
    cal.set(Calendar.DAY_OF_MONTH, day)

    cal.set(Calendar.HOUR_OF_DAY, hour)
    cal.set(Calendar.MINUTE, minute)
    cal.set(Calendar.SECOND, second)
    cal.set(Calendar.MILLISECOND, millisecond)

    return cal.getTime()
}

try {
    def stateManager = context.getStateManager()

    def oldMapRef = stateManager.getState(Scope.LOCAL)
    def oldMap = oldMapRef.toMap()
    def newMap = [:]

    def sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")

    def lookup = context.controllerServiceLookup
    def dbServiceName = dbPoolName.value
    def dbcpServiceId = lookup.getControllerServiceIdentifiers(ControllerService).find { 
        cs -> lookup.getControllerServiceName(cs) == dbServiceName
    }
    def conn = lookup.getControllerService(dbcpServiceId)?.getConnection()
    def sql = new Sql(conn)

    sql.rows("select tablename, timeattr from public.data_export").eachWithIndex { row_outer, idx_outer ->

        def tableName = row_outer[0].toString()
        def timeColumn = row_outer[1].toString()

        def fromTS = oldMap.containsKey(tableName) ? sdf.parse(oldMap[tableName]) : genDate()

        sql.rows(String.format('select max(%s) from %s', timeColumn, tableName)).eachWithIndex { row_inner, idx_inner ->
        
            def toTS = sdf.parse(row_inner[0].toString())

            if (toTS > fromTS) {

                def flowFile = session.create()

                flowFile = session.putAttribute(flowFile, 'tableName', tableName)
                flowFile = session.putAttribute(flowFile, 'timeColumn', timeColumn)

                flowFile = session.putAttribute(flowFile, 'fromTS', sdf.format(fromTS))
                flowFile = session.putAttribute(flowFile, 'toTS', sdf.format(toTS))

                def doc = [:]

                doc.put 'fromTS', fromTS.toString()
                doc.put 'toTS', toTS.toString()

                flowFile = session.write(flowFile, {out -> 
                
                    out.write(JsonOutput.toJson(doc).getBytes())
 
                } as OutputStreamCallback)
        
                session.transfer(flowFile, REL_SUCCESS)
            }

            newMap.put tableName, sdf.format(toTS)
        }
    }


    if (oldMapRef.version == -1) {
        stateManager.setState(newMap, Scope.LOCAL)
    } else {
        stateManager.replace(oldMapRef, newMap, Scope.LOCAL)
    }

} catch(e) {
    log.error('Scripting error', e)
    session.transfer(flowFile, REL_FAILURE)
} finally {
    conn?.close()
}
