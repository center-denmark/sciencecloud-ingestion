import java.util.concurrent.ForkJoinPool
import java.util.concurrent.RecursiveTask;
import java.text.SimpleDateFormat
import org.apache.nifi.controller.ControllerService
import org.apache.nifi.components.state.Scope
import groovy.sql.Sql


public class Interval {
    Date from
    Date to
    Integer count
}

class IntervalProcessor extends RecursiveTask<List<Interval>> {
    Sql sql
    String tableName
    String timeColumn
    Integer limit
    Date from
    Date to

    IntervalProcessor(Sql sql, String tableName, String timeColumn, Integer limit, Date from, Date to) {
        this.sql = sql
        this.tableName = tableName
        this.timeColumn = timeColumn
        this.limit = limit
        this.from = from
        this.to = to
    }

    def genQuery(tableName, timeColumn, from, to) {
        return String.format("select count(*) from %s where %s > '%s' and %s <= '%s'", tableName, timeColumn, from, timeColumn, to)
    }

    def size(intervals) {
        def size = 0

        intervals.each { i -> size += i.count }

        return size
    }

    @Override
    List<Interval> compute() {
        def query = genQuery(tableName, timeColumn, from, to)
        def count = sql.firstRow(query).get("count")

        if (count <= limit) {
            def interval = new Interval()
            interval.from = from
            interval.to = to
            interval.count = count

            return (count > 0) ? [interval] : []
        } else {
            def diff = (to.getTime() - from.getTime()) as long
            def mid = new Date((from.getTime() + diff * 0.5) as long)

            def leftProcessor = new IntervalProcessor(sql, tableName, timeColumn, limit, from, mid)
            def rightProcessor = new IntervalProcessor(sql, tableName, timeColumn, limit, mid, to)

            leftProcessor.fork()
            rightProcessor.fork()

            def leftIntervals = leftProcessor.join()
            def rightIntervals = rightProcessor.join()

            def leftSize = size(leftIntervals)
            def rightSize = size(rightIntervals)

            def result =[]

            if (!leftIntervals.isEmpty())
                result.addAll(leftIntervals)

            if (!rightIntervals.isEmpty())
                result.addAll(rightIntervals)

            return result
        }
    }
}

def conn = null

try {
    def sdf = new SimpleDateFormat('yyyy-MM-dd HH:mm:ss.S')

    def lookup = context.controllerServiceLookup
    def dbServiceName = dbPoolName.value
    def dbcpServiceId = lookup.getControllerServiceIdentifiers(ControllerService).find { 
        cs -> lookup.getControllerServiceName(cs) == dbServiceName
    }
    conn = lookup.getControllerService(dbcpServiceId)?.getConnection()
    def sql = new Sql(conn)

    def inFlowFile = session.get()
    if (!inFlowFile) return

    def tableName = inFlowFile.getAttribute('tableName')
    def timeColumn = inFlowFile.getAttribute('timeColumn')
    def fromTS = inFlowFile.getAttribute('fromTS')
    def toTS = inFlowFile.getAttribute('toTS')

    def from = sdf.parse(fromTS)
    def to = sdf.parse(toTS)

    ForkJoinPool pool = new ForkJoinPool()
    def ip = new IntervalProcessor(sql, tableName, timeColumn, 250_000, from, to)

    pool.execute(ip)
    def result = ip.join()

    result.each { i ->

        outFlowFile = session.create(inFlowFile)

        outFlowFile = session.write(outFlowFile, { out -> 

            def str = String.format("select * from %s where %s > '%s' and %s <= '%s'", tableName, timeColumn, i.from.toString(), timeColumn, i.to.toString())

            out.write(str.getBytes())

        } as OutputStreamCallback)

        outFlowFile = session.putAttribute(outFlowFile, 'fromTS', i.from.toString())
        outFlowFile = session.putAttribute(outFlowFile, 'toTS',   i.to.toString())
        outFlowFile = session.putAttribute(outFlowFile, 'rows',   i.count.toString())

        session.transfer(outFlowFile, REL_SUCCESS)
    }

    session.remove(inFlowFile)
} finally {
    conn?.close()
}

